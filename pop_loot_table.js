/*******************************************************************************
*                           This file is part of:                              *
*                          Path of Paper Loot Roller                           *
********************************************************************************
*  Copyright (C) 2019 - 2020 Nicolas Ivan Milicevic.  All rights reserved.     *
*   * https://gitlab.com/mortykun/pop_loot_roller.git                          *
*                                                                              *
*  Permission is hereby granted, free of charge, to any person obtaining a     *
*  copy of this software and associated documentation files (the "Software"),  *
*  to deal in the Software without restriction, including without limitation   *
*  the rights to use, copy, modify, merge, publish, distribute, sublicense,    *
*  and/or sell copies of the Software, and to permit persons to whom the       *
*  Software is furnished to do so, subject to the following conditions:        *
*                                                                              *
*  The above copyright notice and this permission notice shall be included in  *
*  all copies or substantial portions of the Software.                         *
*                                                                              *
*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  *
*  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    *
*  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL     *
*  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER  *
*  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     *
*  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         *
*  DEALINGS IN THE SOFTWARE.                                                   *
*******************************************************************************/

loot_table_item_type = {
    X       : { name: '[Random]',   weight:   0 },
    amulet  : { name: 'Amulet',     weight:  50 },
    armor   : { name: 'Armor',      weight: 100 },
    boots   : { name: 'Boots',      weight: 100 },
    flask   : { name: 'Flask',      weight:  20 },
    helm    : { name: 'Helm',       weight: 100 },
    orb     : { name: 'Orb',        weight:  20 },
    ring    : { name: 'Ring',       weight:  75 },
    shield  : { name: 'Shield',     weight: 100 },
    weapon  : { name: 'Weapon',     weight: 100 }
};

loot_table_sub_type = {
  amulet : {
    X     : { name: '[Random]',         weight: 0 },
    amber : { name: 'Amber Amulet',     weight: 100, impl: [ { mod: 'str',    roll: '1d10', add_tier: 5 }  ] },
    coral : { name: 'Coral Amulet',     weight: 100, impl: [ { mod: 'life',   roll: '2d10', add_tier: 10 } ] },
    lapis : { name: 'Lapis Amulet',     weight: 100, impl: [ { mod: 'int',    roll: '1d10', add_tier: 5 }  ] },
    jade  : { name: 'Jade Amulet',      weight: 100, impl: [ { mod: 'dex',    roll: '1d10', add_tier: 5 }  ] },
    onxy  : { name: 'Onxy Amulet',      weight: 100, impl: [ { mod: 'resist', roll: '1d4',  add_tier: 2 }  ] },
    paua  : { name: 'Paua Amulet',      weight: 100, impl: [ { mod: 'mana',   roll: '1d4',  add_tier: 2 }  ] }
  },
  armor  : {
    X        : { name: '[Random]',      weight: 0 },
    cloth    : { name: 'Cloth Armor',   weight: 100, impl: [ { mod: 'es',     per_tier : 20 } ] },
    leather  : { name: 'Leather Armor', weight: 100, impl: [ { mod: 'resist', per_tier : 4  } ] },
    plate    : { name: 'Plate Armor',   weight: 100, impl: [ { mod: 'armor',  per_tier : 6  }, { mod: 'sk_ath', val: -4}, { mod: 'sk_man', val: -4} ] }
  },
  boots  : {
    X        : { name: '[Random]',      weight: 0 },
    cloth    : { name: 'Cloth Boots',   weight: 100, impl: [ { mod: 'armor',  per_tier: 2  } ] },
    leather  : { name: 'Leather Boots', weight: 100, impl: [ { mod: 'resist', per_tier: 2  } ] },
    plate    : { name: 'Plate Boots',   weight: 100, impl: [ { mod: 'es',     per_tier: 10 } ] }
  },
  flask : {
    flask  : { name: 'Flask', weight: 100, impl: [ { mod: 'flask_chrg', val: 10 } ] }
  },
  helm  : {
    X        : { name: '[Random]',     weight  : 0 },
    cloth    : { name: 'Cloth Helm',   weight: 100, impl: [ { mod: 'armor',  per_tier: 2  } ] },
    leather  : { name: 'Leather Helm', weight: 100, impl: [ { mod: 'resist', per_tier: 2  } ] },
    plate    : { name: 'Plate Helm',   weight: 100, impl: [ { mod: 'es',     per_tier: 10 } ] }
  },
  orb : {
    X               : { name: '[Random]',           weight: 0 },
    chaos           : { name: 'Chaos Orb',          weight: 100 },
    divine          : { name: 'Divine Orb',         weight: 50 },
    exalt           : { name: 'Exalt Orb',          weight: 10 },
    transmutation   : { name: 'Transmutation Orb',  weight: 1000 }
  },
  ring   : {
    X       : { name: '[Random]',   weight: 0 },
    coral   : { name: 'Coral Ring', weight: 100, impl: [ { mod: 'life',   roll: '2d10', add_tier: 10 } ] },
    paua    : { name: 'Paua Ring',  weight: 100, impl: [ { mod: 'mana',   roll: '1d4', add_tier: 2 } ] },
    iron    : { name: 'Iron Ring',  weight: 100, impl: [ { mod: 'wam',    per_tier: 1 } ] },
    onyx    : { name: 'Onyx Ring',  weight: 100, impl: [ { mod: 'resist', roll: '1d4', add_tier: 2 } ] }
  },
  shield : {
    X         : { name: '[Random]',       weight: 0 },
    voodoo    : { name: 'Voodoo Shield',  weight: 100, impl: [ { mod: 'block', val: 1 }, { mod: 'es',     per_tier: 20, }, { mod: 'dmg', per_tier: 2 } ] },
    leather   : { name: 'Leather Shield', weight: 100, impl: [ { mod: 'block', val: 1 }, { mod: 'resist', per_tier: 6   } ] },
    metal     : { name: 'Metal Shield',   weight: 100, impl: [ { mod: 'block', val: 1 }, { mod: 'armor',  per_tier: 8   }, { mod: 'sk_ath', val: -2 }, { mod: 'sk_man', val: -2 } ] }
  },
  weapon : {
    X         : { name: '[Random]',           weight: 0 },
    axe       : { name: 'One Handed Axe',     weight: 100, hands: 1, impl: [ { mod: 'wam', per_tier: 1 }, { text: '+1 Damage per 1 WAM' } ] },
    axe2h     : { name: 'Two Handed Axe',     weight: 100, hands: 2, impl: [ { mod: 'wam', per_tier: 3 }, { text: '+1 Damage per 1 WAM' } ] },
    bow       : { name: 'Bow',                weight: 100, hands: 2, impl: [ { mod: 'wam', per_tier: 3 }, { text: 'Roll Initiative Twice' } ] },
    claw      : { name: 'Claw',               weight: 100, hands: 1, impl: [ { mod: 'wam', per_tier: 1 }, { mod: 'lifeleech', per_tier: 4 } ] },
    dagger    : { name: 'Dagger',             weight: 100, hands: 1, impl: [ { mod: 'wam', per_tier: 1 }, { mod: 'crit_rng', val: 1 } ] },
    mace      : { name: 'One Handed Mace',    weight: 100, hands: 1, impl: [ { mod: 'wam', per_tier: 1 }, { text: '-2 Speed to Enemies you Stun' } ] },
    mace2h    : { name: 'Two Handed Mace',    weight: 100, hands: 2, impl: [ { mod: 'wam', per_tier: 3 }, { text: '-2 Speed to Enemies you Stun' } ] },
    scept     : { name: 'Scepter',            weight: 100, hands: 1, impl: [ { mod: 'wam', per_tier: 1 }, { text: '+1 Elemental Threshhold Duration' } ] },
    spear     : { name: 'One Handed Spear',   weight: 100, hands: 1, impl: [ { mod: 'wam', per_tier: 1 }, { text: 'Roll Manuever or Athletics Twice' } ] },
    polarm    : { name: 'Two Handed Polearm', weight: 100, hands: 2, impl: [ { mod: 'wam', per_tier: 3 }, { text: 'Roll Manuever or Athletics Twice' } ] },
    staff     : { name: 'Staff',              weight: 100, hands: 2, impl: [ { mod: 'wam', per_tier: 3 }, { mod: 'block', val: 1 }] } ,
    sword     : { name: 'One Handed Sword',   weight: 100, hands: 1, impl: [ { mod: 'wam', per_tier: 1 }, { mod: 'crit_rng', val: 1 } ] },
    sword2h   : { name: 'Two Handed Sword',   weight: 100, hands: 2, impl: [ { mod: 'wam', per_tier: 3 }, { mod: 'crit_rng', val: 1 } ] },
    wand      : { name: 'Wand',               weight: 100, hands: 1, impl: [ { mod: 'wam', per_tier: 1 }, { mod: 'mana', per_tier: 2, }, { text: 'Convert Elemental Damage to Element of your Choice' } ] }
  }
};

loot_table_rarity = {
    X          : { name: '[Random]',   weight: 0 },
    normal     : { name: 'Normal',     weight: 70 },
    magic      : { name: 'Magic',      weight: 20 },
    rare       : { name: 'Rare',       weight: 10 },
    signature  : { name: 'Signature',  weight: 1 }
};


loot_table_tier = {
    X    : { name: '[Random]', weight: 0 },
    1    : { name: 'Tier 1',   weight: 100 },
    2    : { name: 'Tier 2',   weight: 100 },
    3    : { name: 'Tier 3',   weight: 100 }
};

loot_table_affixes = {
    str : { mod: 'str', roll: '1d10', add_tier: 5, weight: 100, prefix_name: { 1: "Brute's",  2: "Bear's",    3: "Titan's" },   suffix_name: { 1: 'of the Wrestler', 2: 'of the Lion',    3: 'of the Goliath' } },
    dex : { mod: 'dex', roll: '1d10', add_tier: 5, weight: 100, prefix_name: { 1: "Fox's",    2: "Panther's", 3: "Phantom's" }, suffix_name: { 1: 'of the Lynx',     2: 'of the Falcon',  3: 'of the Wind' } },
    int : { mod: 'int', roll: '1d10', add_tier: 5, weight: 100, prefix_name: { 1: "Pupil's",  2: "Augar's",   3: "Savant's" },  suffix_name: { 1: 'of the Student',  2: 'of the Prodigy', 3: 'of the Sage' } },
    dmg : { mod: 'dmg', roll: '1d4',  add_tier: 2, weight: 100, prefix_name: { 1: 'Spiteful', 2: 'Vicous',    3: 'Wicked'   },  suffix_name: { 1: 'of Spite',        2: 'of Vicousness',  3: 'of Wickedness' } },

    thr_dur:   { mod: 'thr_dur', val: 1,                       weight: 100, prefix_name: { 1: "Suffering" },                                        suffix_name: { 1: 'of Suffering' } },
    thr_lwr:   { mod: 'ene_thr', roll: '-1d10', add_tier: -6,  weight: 100, prefix_name: { 1: "Bruiser's",  2: "Wounder's",    3: "Lacerator's" },  suffix_name: { 1: 'of Brusing', 2: 'of Wounding', 3: 'of Lacerating' } },
    wam:       { mod: 'wam', per_tier: 1,                      weight: 100, prefix_name: { 1: "Reaver's",   2: "Champion's",   3: "Emperor's"},     suffix_name: { 1: 'of the Reaver', 2: 'of the Chamption', 3: 'of the Emperor' } },
    lifeleech: { mod: 'lifeleech', per_tier: 2,                weight: 100, prefix_name: { 1: 'Successful', 2: 'Victorious',   3: 'Truimphating' }, suffix_name: { 1: 'of Success',    2: 'of Victory',       3: 'of Truimph' } },
    manaleech: { mod: 'manaleech', per_tier: 2,                weight: 100, prefix_name: { 1: 'Absorpting', 2: 'Osomosifying', 3: 'Consuming' },    suffix_name: { 1: 'of Absorption', 2: 'of Osmosis',       3: 'of Consumption' } },

    thr_incr:   { mod: 'thr',    roll: '1d10', add_tier: 6,   weight: 100, prefix_name: { 1: 'Thick',      2: 'Stone',      3: 'Iron' },       suffix_name: { 1: 'of Thick Skin',   2: 'of Stone Skin',  3: 'of Iron Skin' } },
    resist:     { mod: 'resist', roll: '1d4',  add_tier: 2,   weight: 100, prefix_name: { 1: 'Eviction',   2: 'Drake',      3: 'Kiln' },       suffix_name: { 1: 'of Eviction',     2: 'of the Drake',   3: 'of the Kiln' } },
    life:       { mod: 'life',   roll: '2d10', add_tier: 10,  weight: 100, prefix_name: { 1: 'Healthy',    2: 'Stalwart',   3: 'Prime' },      suffix_name: { 1: 'of Health',       2: 'of Virility',    3: 'of Rapture' } },
    armor:      { mod: 'armor',  roll: '1d4',  add_tier: 2,   weight: 100, prefix_name: { 1: 'Studded',    2: 'Plated',     3: 'Fortified' },  suffix_name: { 1: 'of Studds',       2: 'of Plate',       3: 'of Foritifcation' } },
    ele_res:    { mod: 'resist', roll: '1d4',  add_tier: 2,   weight: 100, prefix_name: { 1: 'Eviction',   2: 'Drake',      3: 'Kiln' },       suffix_name: { 1: 'of Eviction',     2: 'of the Drake',   3: 'of the Kiln' } },
    es:         { mod: 'es',     roll: '2d10', add_tier: 10,  weight: 100, prefix_name: { 1: 'Prtoective', 2: 'Resolute',   3: 'Idomitable' }, suffix_name: { 1: 'of Will',         2: 'of Bravery',     3: 'of Daunting' } },
    sk_man:     { mod: 'sk_man', roll: '1d10', add_tier: 5,   weight: 100, prefix_name: { 1: 'Agile',      2: 'Fleet',      3: 'Blurred' },    suffix_name: { 1: 'of Dancing',      2: 'of Acrobatics',  3: 'of Phasing' } },
    spd:        { mod: 'spd',    per_tier: 1,                 weight: 100, prefix_name: { 1: "Runner's",   2: "Stallion's", 3: "Cheetah's" },  suffix_name: { 1: 'of the Sprinter', 2: 'of the Gazelle', 3: 'of the Hellion' } },
    mana:       { mod: 'mana',   per_tier: 1,                 weight: 100, prefix_name: { 1: 'Azure',      2: 'Aqua',       3: 'Mazarine' },   suffix_name: { 1: 'of Cobalt',       2: 'of Sapphire',    3: 'of Zaffre' } },

    crit_dmg:   { mod: 'crit_dmg', per_tier: 10,                                 weight: 100, prefix_name: { 1: 'Needling', 2: 'Piercing',       3: 'Rending'  }, suffix_name: { 1: 'of Stining', 2: 'of Rupturing', 3: 'of Incision' } },
    st_spd:     { text: '-1 Speed per tier to Stunned Enemies',                  weight: 100, prefix_name: { 1: 'Dazing',   2: 'Disorientating', 3: 'Stunning' }, suffix_name: { 1: 'of Dazing', 2: 'of Disorientation', 3: 'of Stunning' } },
    shock_dmg:  { text: '+1 WAM per tier For You and allies vs Shocked Enemies', weight: 100, prefix_name: { 1: 'Rage' },                                         suffix_name: { 1: 'of Ferocity' } },

    flask_greedy:     { name: 'Greedy',     weight: 100, prefix_name: { 1: 'Greedy'     }, suffix_name: { 1: 'of Greed'         }, text: 'Double your healing and mana regeneration per use. This flask uses ten charges' },
    flask_ample:      { name: 'Ample',      weight: 100, prefix_name: { 1: 'Ample'      }, suffix_name: { 1: 'of Ampleness'     }, text: 'Extra 5 maximum charge', mod: 'flask_chrg', val: 5 },
    flask_deadly:     { name: 'Deadly',     weight: 100, prefix_name: { 1: 'Deadly'     }, suffix_name: { 1: 'of Death'         }, text: 'Regain 1 flask charge on a crit' },
    flask_bully:      { name: 'Bully',      weight: 100, prefix_name: { 1: 'Bully'      }, suffix_name: { 1: 'of the Bully'     }, text: 'Increases your Athletics by 5 per tier for 1 round' },
    flask_granite:    { name: 'Granite',    weight: 100, prefix_name: { 1: 'Granite'    }, suffix_name: { 1: 'of Granite'       }, text: 'Doubles your Armour for 1 round. Does not regenerate Life or Mana and costs 8 charges per use', unique_grp: 'flask_sp' },
    flask_thawing:    { name: 'Thawing',    weight: 100, prefix_name: { 1: 'Thawing'    }, suffix_name: { 1: 'of Thawing'       }, text: 'Removes frozen effect' },
    flask_general:    { name: 'General',    weight: 100, prefix_name: { 1: 'General'    }, suffix_name: { 1: 'of the General'   }, text: 'Also heals minions equal to their Threshold' },
    flask_shield:     { name: 'Shield',     weight: 100, prefix_name: { 1: 'Shield'     }, suffix_name: { 1: 'of Shielding'     }, text: 'Doubles your Resistance for 1 round. Does not regenerate Life or Mana and costs 8 charges per use', unique_grp: 'flask_sp' },
    flask_slippery:   { name: 'Slippery',   weight: 100, prefix_name: { 1: 'Slippery'   }, suffix_name: { 1: 'of Slipperyness'  }, text: 'Increases your Maneuver by 5 per tier for 1 round' },
    flask_blessed:    { name: 'Blessed',    weight: 100, prefix_name: { 1: 'Blessed'    }, suffix_name: { 1: 'of Blessedness'   }, text: 'Removes all curses' },
    flask_clarity:    { name: 'Clarity',    weight: 100, prefix_name: { 1: 'Clarity'    }, suffix_name: { 1: 'of Clarity'       }, text: 'Remove any stun effect' },
    flask_dousing:    { name: 'Dousing',    weight: 100, prefix_name: { 1: 'Dousing'    }, suffix_name: { 1: 'of Dousing'       }, text: 'Remove any burning effect' },
    flask_rubber:     { name: 'Rubber',     weight: 100, prefix_name: { 1: 'Rubber'     }, suffix_name: { 1: 'of Rubber'        }, text: 'Remove any shock effect' },
    flask_supportive: { name: 'Supportive', weight: 100, prefix_name: { 1: 'Supportive' }, suffix_name: { 1: 'of Support'       }, text: 'This flask only requires a Move action to use on another instead of a Major action' }
};

loot_table_item_type_affixes = {
    amulet: { afx: [ 'str', 'dex', 'int', 'st_spd', 'shock_dmg', 'lifeleech', 'manaleech', 'armor', 'life', 'crit_dmg', 'wam' ] },
    armor:  { afx: [ 'str', 'dex', 'int', 'thr_incr', 'resist', 'life' ] , sub: {'plate': ['armor'], 'leather': ['ele_res'], 'cloth': ['es'] } },
    boots:  { afx: [ 'str', 'dex', 'int', 'thr_incr', 'resist', 'life', 'spd', 'sk_man' ], sub: {'plate': ['armor'], 'leather': ['ele_res'], 'cloth': ['es'] } },
    helm:   { afx: [ 'str', 'dex', 'int', 'thr_incr', 'resist', 'life' ], sub: {'plate': ['armor'], 'leather': ['ele_res'], 'cloth': ['es'] } },
    ring:   { afx: [ 'str', 'dex', 'int', 'dmg', 'lifeleech', 'resist', 'life', 'armor', 'wam'  ] },
    shield: { afx: [ 'str', 'dex', 'int', 'thr_incr', 'resist', 'life' ], sub: {'plate': ['metal'], 'leather': ['ele_res'], 'voodoo': ['es'] } },
    weapon: { afx: [ 'str', 'dex', 'int', 'thr_dur', 'thr_lwr', 'wam', 'lifeleech', 'manaleech' ] },
    flask:  { afx: [ 'flask_greedy', 'flask_ample', 'flask_deadly', 'flask_bully', 'flask_granite', 'flask_thawing', 'flask_general', 'flask_shield', 'flask_slippery', 'flask_blessed', 'flask_clarity', 'flask_dousing', 'flask_rubber', 'flask_supportive' ] }
};

loot_table_mod = {
    str  : { name: 'Strength' },
    dex  : { name: 'Dexterity' },
    int  : { name: 'Intelligence' },
    dmg  : { name: 'Damage' },
    mana : { name: 'Mana' },

    thr_dur:   { name: 'Threshold Duration' },
    thr:       { name: 'Threshold' },
    ene_thr:   { name: 'Enemy Threshold' },
    wam:       { name: 'WAM' },
    lifeleech: { name: 'Life Leech' },
    manaleech: { name: 'Mana Leech' },

    resist:    { name: 'Resistance' },
    life:      { name: 'Life' },
    armor:     { name: 'Armor' },
    es:        { name: 'Energy Shield' },
    spd:       { name: 'Speed' },
    block:     { name: 'Block Range' },

    crit_dmg:   { name: 'Critical Damage' },
    crit_rng:   { name: 'Critical Strike Range' },

    flask_chrg: { name: 'Flask Charges' },

    sk_ath:  { name: 'Athletics' },
    sk_end:  { name: 'Endurance' },
    sk_sur:  { name: 'Survival' },
    sk_ste:  { name: 'Stealth' },
    sk_sou:  { name: 'Soul Manipulation' },
    sk_man:  { name: 'Maneuver' },
    sk_ini:  { name: 'Initiative' },
    sk_per:  { name: 'Perception' },
    sk_lor:  { name: 'Lore' },
    sk_sou:  { name: 'Soulcraft' },
    sk_neg:  { name: 'Negotiation' },
    sk_cha:  { name: 'Charm' },
    sk_int:  { name: 'Intimidate' },
};

loot_table_signature_mods = {
    ghost            : { name: 'Ghost',            weight: 100 },
    ogre             : { name: 'Ogre',             weight: 100 },
    soul_hunter      : { name: 'Soul Hunter',      weight: 100 },
    spider           : { name: 'Spider',           weight: 100 },
    feral            : { name: 'Feral',            weight: 100 },
    intelligent_item : { name: 'Intelligent Item', weight: 100 },
    pre_cognisant    : { name: 'Pre-cognisant',    weight: 100 },
    camouflaged      : { name: 'Camouflaged',      weight: 100 },
    water_walking    : { name: 'Water Walking',    weight: 100 },
    blessed          : { name: 'Blessed',          weight: 100 },
    lucky            : { name: 'Lucky',            weight: 100 }
};
