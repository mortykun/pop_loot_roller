# Path of Paper Loot Roller

This is a basic HTML/Javascript program that generates items for the Path
of Paper game. see... https://docs.google.com/document/d/1fhEGY_BiXG2KnF4dxSFxllqVffYg1nKtOm_lmPb0xUU/edit#

## Installation Instruction

Download the `pop_loot_roller.html` and `pop_loot_table.js` files on your pc and
open the `pop_loot_roller.html` in any web browser that supports java script.

Feel free to mess with the loot table as you see fit :)

Have a nice day.
